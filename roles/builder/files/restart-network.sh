#!/bin/sh

if ! /bin/ping -c1 gitlab.com; then
    /bin/systemctl restart systemd-networkd
fi
