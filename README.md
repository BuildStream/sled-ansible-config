## Deployement

This is the command to deploy gitlab runners on the sleds:

```
ansible-playbook -i hosts playbook.yml --vault-password-file=password_file
```

## Getting access

To get access to the sleds, please request you ssh public key to be
added. Please contact operations@codethink.co.uk.

In order to run ansible or edit some of the files, you will need the
vault password file. Please contact valentin.david@codethink.co.uk.
